
struct Point(T= ulong){
    T x, y;
}
struct FieldRange(T = ulong){
    T maxX;
    T maxY;

    T x = 0;
    T y = 0;
    
    this(T maxX, T maxY){
        this.maxX = maxX;
        this.maxY = maxY;
    }
    bool empty(){
        return x == maxX && y == maxY;
    }
    Point!T front(){
        return Point!T(x,y);
    }
    void popFront(){
        y++;
        if (y==maxY&&x!= maxX){
            x++;
            y = 0;
        }
    }

}

struct LifeField{
    ulong height;
    ulong width;

    bool[] front; 
    bool[] back;

    this(ulong h, ulong w){
        height = h;
        width = w;
        front = new bool[h*w];
        back = new bool[h*w];
    }

    @property
    private ulong h(){
        return height;
    }

    @property
    private ulong w(){
        return width;
    }

    private bool index(bool[] array, ulong i, ulong j){
        if( 0 <= i && i < height && 0 <= j && j < width)
            return array[i*w+j];
        else
            return 0;
    }

    private auto index(bool[] array, ulong i, ulong j, bool value){
        if( 0 <= i && i < height && 0 <= j && j < width)
            array[i*w+j] = value;
    }

    bool opIndex(ulong i, ulong j){
        return index(front,i,j);
    }
    auto opIndexAssign( bool value, ulong i, ulong j){
        return index(front,i,j, value);
    }

    auto swap(){
        import std.algorithm: swap;
        swap(front, back);
    }

    auto nextFrame(){
        import std: parallel;
        foreach(p; FieldRange!ulong(height, width).parallel){
            immutable i = p.x;
            immutable j = p.y;
            immutable _neighbors = neighbors(i, j);
            if(_neighbors > 3 || _neighbors < 2){
                this.index(back, i, j, false);
            }else if(_neighbors == 3){
                this.index(back, i, j, true);
            }
            else{
                this.index(back, i, j, this[i,j]);
            }
        }
    }

    ulong neighbors(ulong i, ulong j){
        ulong _neighbors;
        static foreach(iShift; [-1,0,1]){
            static foreach(jShift; [-1,0,1]){
                static if(iShift != 0 || jShift != 0)
                    if(this[i+iShift, j+jShift]) _neighbors++;
            }
        }
        return _neighbors;
    }
    auto draw(){
        import deimos.ncurses: COLS, LINES, mvaddch;
        foreach(i; 0..COLS){
            foreach(j; 0..LINES){ 
                if (this[i+20,j+20])
                    mvaddch(j,i,'#');
            }
        }
    }
    auto opIndexAssign( string value, ulong I, ulong J){
        import std:split;

        foreach(i, line; value.split('\n')){
            foreach(j, s; line){
                if (s != ' ' && s != '\0'){
                    this[J+j, I+i] = true;
                }
            }
        }
    }
}

unittest{
    import std;
    foreach(p; FieldRange!ulong(10,20)) p.writeln;
}
