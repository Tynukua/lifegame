import std:to, Duration;
import core.thread: MonoTime, Thread, dur;

import deimos.ncurses;

import lifefield: LifeField;

void main()
{
    auto f = LifeField(1000,1000);
    initscr();
    curs_set(0);
    noecho();
    timeout(0);
    keypad(stdscr, true);
    f[30,30] = import("glider.txt");
    while(1){
        auto start = MonoTime.currTime;
        clear;
        f.nextFrame;
        f.swap;
        f.draw;
        refresh;
        auto end = MonoTime.currTime;
        auto frametime = 50.dur!"msecs" - (end-start).to!Duration;
        if(!frametime.isNegative)
            Thread.sleep(frametime);
        
    }
}
